# HunterTurcin.RandomBat

This is a random bat image service. Images can be served or uploaded through a web app or a public API.

Uploaded images must be manually approved by setting `BatImages.Status` in the database to `1`.

## Building and running

[ASP.NET Core 5.0](https://dotnet.microsoft.com/download/dotnet/5.0) is required.

While developing:

```sh
dotnet run
```

Preparing a deployment:

```sh
dotnet publish --configuration Release
cd bin/Release/net5.0/
dotnet HunterTurcin.RandomBat.dll
```
