namespace HunterTurcin.RandomBat
{
    public enum SubmissionStatus
    {
        Uploaded,
        Approved,
    }
}
