namespace HunterTurcin.RandomBat
{
    public class BatImage
    {
        public int Id { get; set; }
        public string Extension { get; set; }
        public SubmissionStatus Status { get; set; }
        public byte[] Content { get; set; }
    }
}
