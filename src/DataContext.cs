using Microsoft.EntityFrameworkCore;

namespace HunterTurcin.RandomBat
{
    public class DataContext : DbContext
    {
        public DbSet<BatImage> BatImages { get; set; }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }
    }
}
