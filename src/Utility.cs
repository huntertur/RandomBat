using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace HunterTurcin.RandomBat
{
    public static class Utility
    {
        public static readonly Dictionary<string, string> EXTENSIONS = new Dictionary<string, string>()
        {
            ["bmp"] = "image/bmp",
            ["gif"] = "image/gif",
            ["jpeg"] = "image/jpeg",
            ["jpg"] = "image/jpeg",
            ["png"] = "image/png",
            ["svg"] = "image/svg+xml",
            ["tif"] = "image/tiff",
            ["tiff"] = "image/tiff",
            ["webp"] = "image/webp",
        };

        private static readonly Random _random = new Random();

        public static async Task<string> RandomImageUrl(DataContext context)
        {
            var count = await ApprovedCount(context);
            var image = (await context.BatImages.Where(image => image.Status == SubmissionStatus.Approved)
                                                .ToListAsync())
                                                .ElementAtOrDefault(_random.Next(0, count));

            return image switch
            {
                null => null,
                _ => $"api/get/{image.Id}.{image.Extension}",
            };
        }

        public static async Task<int> ApprovedCount(DataContext context)
        {
            return await context.BatImages.Where(image => image.Status == SubmissionStatus.Approved)
                                          .CountAsync();
        }

        public static async Task<int> UploadedCount(DataContext context)
        {
            return await context.BatImages.Where(image => image.Status == SubmissionStatus.Uploaded)
                                          .CountAsync();
        }

        /// Returns false if the image has an unsupported file extension.
        public static async Task<bool> UploadImage(DataContext context, IFormFile file)
        {
            var extension = file.FileName.Split('.').Last().ToLower();

            if (!EXTENSIONS.ContainsKey(extension))
            {
                return false;
            }

            using var stream = file.OpenReadStream();
            var content = new byte[stream.Length];
            await stream.ReadAsync(content);

            context.Add(new BatImage
            {
                Extension = extension,
                Status = SubmissionStatus.Uploaded,
                Content = content,
            });

            await context.SaveChangesAsync();

            return true;
        }
    }
}
