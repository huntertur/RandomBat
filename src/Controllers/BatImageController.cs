using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace HunterTurcin.RandomBat
{
    [ApiController]
    [Route("api")]
    public class BatImageController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly Random _random = new Random();

        public BatImageController(DataContext context)
        {
            _context = context;
        }

        [HttpPost]
        [Route("upload")]
        public async Task<IActionResult> Upload(IFormFile file)
        {
            if (await Utility.UploadImage(_context, file))
            {
                return Accepted();
            }

            return new UnsupportedMediaTypeResult();
        }

        [HttpGet]
        [Route("get/{id}.{extension}")]
        public async Task<IActionResult> GetSpecific(int id, string extension)
        {
            var image = await _context.BatImages.Where(image => image.Id == id && image.Extension == extension)
                                                .FirstOrDefaultAsync();

            if (image == null)
            {
                return NotFound();
            }

            if (image.Status != SubmissionStatus.Approved)
            {
                return Unauthorized();
            }

            return File(image.Content, Utility.EXTENSIONS[extension]);
        }

        [HttpGet]
        [Route("random")]
        public async Task<IActionResult> GetRandom()
        {
            var url = await Utility.RandomImageUrl(_context);

            return url switch
            {
                null => NoContent(),
                _ => Ok(url),
            };
        }
    }
}
