using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace HunterTurcin.RandomBat.Pages
{
    public class UploadModel : PageModel
    {
        private readonly DataContext _context;

        public string Message { get; set; }

        public UploadModel(DataContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            Message = null;
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(IFormFile file)
        {
            if (file == null)
            {
                Message = "No image was selected.";
            }
            else if (!await Utility.UploadImage(_context, file))
            {
                Message = "The image had an unsupported extension.";
            }
            else
            {
                Message = "The image was uploaded.";
            }

            return Page();
        }
    }
}
