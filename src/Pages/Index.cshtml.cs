using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace HunterTurcin.RandomBat.Pages
{
    public class IndexModel : PageModel
    {
        private readonly DataContext _context;

        public string ImageUrl { get; set; }
        public int ApprovedCount { get; set; }
        public int UploadedCount { get; set; }

        public IndexModel(DataContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> OnGetAsync()
        {
            ImageUrl = await Utility.RandomImageUrl(_context);
            ApprovedCount = await Utility.ApprovedCount(_context);
            UploadedCount = await Utility.UploadedCount(_context);

            return Page();
        }
    }
}
